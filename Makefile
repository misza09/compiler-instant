compiler:
	ghc --make ./src/jvm.hs -idir1:./src -o jvm-instant
	ghc --make ./src/llvm.hs -idir1:./src -o llvm-instant
all:
	happy -gca ./src/ParInstant.y
	alex -g ./src/LexInstant.x
	latex ./src/DocInstant.tex; dvips ./src/DocInstant.dvi -o ./src/DocInstant.ps
	ghc --make ./src/TestInstant.hs -o ./src/TestInstant
clean:
	-rm -f *.log *.aux *.hi *.o *.dvi
	-rm -f src/DocInstant.ps
distclean: clean
	-rm -f DocInstant.* LexInstant.* ParInstant.* LayoutInstant.* SkelInstant.* PrintInstant.* TestInstant.* AbsInstant.* TestInstant ErrM.* SharedString.* Instant.dtd XMLInstant.* Makefile*

