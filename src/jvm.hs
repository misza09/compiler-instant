module Main where
import System.IO
import System.Environment
import qualified Data.Map as M
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Data.Array
import Data.Maybe
import LexInstant 
import ParInstant 
import AbsInstant 
import ErrM
type VarMap = M.Map String Integer
type Computation = StateT VarMap IO

header = ".super java/lang/Object\n"
		++ "\n"
		++ ".method <init>()V\n"
        ++ "  .limit stack 1\n"
		++ "  .limit locals 1\n"
		++ "  .line 1\n"
		++ "  aload_0\n"
        ++ "  invokespecial java/lang/Object/<init>()V\n"
		++ "  return\n"
        ++ ".end method\n"
		++ "\n"
		++".method public static main([Ljava/lang/String;)V\n"

footer = "return\n"
	++ ".end method\n"

javaGetPrint = "getstatic java/lang/System/out Ljava/io/PrintStream;" ++ "\n" 
javaPrint = "invokevirtual java/io/PrintStream/println(I)V" ++ "\n"


getVarPos :: String -> Computation Integer
getVarPos name = do
	varNum <- gets (\vars -> M.lookup name vars) 
	size <- gets (\vars -> M.size vars)
	case varNum of
		Just x -> return x
		otherwise -> return (toInteger size + 1)

getStoreCommand :: Integer -> String
getStoreCommand idx
	|idx < 4 = "istore_" ++ show idx ++ "\n"
	|otherwise = "istore " ++ show idx ++ "\n"

getLoadCommand :: Integer -> String
getLoadCommand idx
	|idx < 4 = "iload_" ++ show idx ++ "\n"
	|otherwise = "iload " ++ show idx ++ "\n"


between :: Integer -> Integer -> Integer -> Bool
between n b e = (n >= b) && (n <= e)

getConstCommand :: Integer -> String
getConstCommand num
	|num == -1 = "iconst_m1"
	|(between num 0 5) = "iconst_" ++ show num
	|(between num (-128) 127) = "bipush " ++ show num
	|(between num (-32768) 32767) = "sipush " ++ show num
	|otherwise = "ldc " ++ show num


assignVar :: String -> Computation String
assignVar name = do
	vars <- get
	len <- getVarPos name
	put ((M.insert name len) vars)
	return  (getStoreCommand len)

getVar :: String -> Computation String
getVar name = do
	varNum <- gets (\vars -> M.lookup name vars) 
	case varNum of
		Just id -> return  (getLoadCommand id)
		otherwise -> error ("Undeclared value for "++ name)


getHeight a b = max a (b+1)

getExpVal :: Exp -> Computation (String, Integer)

getExpVal (ExpLit val) = do
		return ((getConstCommand val)++"\n", 1)


getExpVal (ExpAdd a b) = do
	(aResult, aStack) <- getExpVal a 
	(bResult, bStack) <- getExpVal b
	if aStack > bStack then
		return (aResult ++ bResult ++ "iadd" ++ "\n", getHeight aStack bStack)
	else
		return (bResult ++ aResult ++ "iadd" ++ "\n", getHeight bStack aStack)

getExpVal (ExpSub a b) = do
	(aResult, aStack)  <- getExpVal a 
	(bResult, bStack)  <- getExpVal b
	return (aResult ++ bResult ++ "isub" ++ "\n", getHeight aStack bStack)

getExpVal (ExpMul a b) = do
	(aResult, aStack)  <- getExpVal a 
	(bResult, bStack)  <- getExpVal b
	if aStack > bStack then
		return (aResult ++ bResult ++ "imul" ++ "\n", getHeight aStack bStack)
	else
		return (bResult ++ aResult ++ "imul" ++ "\n", getHeight bStack aStack)

getExpVal (ExpDiv a b) = do
	(aResult, aStack)  <- getExpVal a 
	(bResult, bStack)  <- getExpVal b
	return (aResult ++ bResult ++ "idiv" ++ "\n", getHeight aStack bStack)

getExpVal (ExpVar (Ident name)) = do
	var <- getVar name
	return (var, 1)
		
	

compute :: [Stmt] -> String -> Integer -> Computation()
compute [] result stack = do
	size <- gets (\vars -> M.size vars)
	liftIO $ putStrLn $ (header 
		++ ".limit stack " ++ show stack ++ "\n" 
		++ ".limit locals " ++ show (size + 1) ++ "\n" 
		++ result ++ "\n"
		++ footer)
	return ()

compute (a:l) result stack = do
	(goResult, goStack) <- go a
	compute l (result ++ goResult) (max stack goStack)
	where
		go (SAss (Ident name) exp) = do
			(expResult, expStack) <- getExpVal exp
		 	assResult <- assignVar name
			return (expResult ++ assResult, expStack)
		go (SExp expr) = do
			(expResult, expStack) <-getExpVal expr
			return (javaGetPrint ++ expResult ++ javaPrint , (expStack+1))


runProgram :: String -> IO()
runProgram s=
	case pProgram $ myLexer s of
	Bad a -> putStrLn a
	Ok (Prog t) ->
		evalStateT (compute t "" 0) (M.empty)
		
main :: IO()
main = do
	name <- getProgName
	args <- getArgs
	case args of
		[fp] -> readFile fp >>= runProgram
		_ -> getContents >>= runProgram




