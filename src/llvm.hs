module Main where
import System.IO
import System.Environment
import qualified Data.Set as S
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Data.Array
import Data.Maybe
import LexInstant 
import ParInstant 
import AbsInstant 
import ErrM
type VarMap = S.Set String
type Computation = StateT VarMap IO

header = "@.str = private unnamed_addr constant [4 x i8] c\"%d\\0A\\00\", align 1\n"
	++ "define i32 @main() #0 {\n"
	++ "%1 = alloca i32, align 4\n"

footer = "ret i32 0\n"
	++ "}\n"
	++ "declare i32 @printf(i8*, ...) #1\n"


assignVar :: String -> Computation String
assignVar name = do
	vars <- get
	if S.member name vars then
		return ""
	else
		put (S.insert name vars) >> return  ("%" ++ name ++ " = alloca i32, align 4" ++ "\n") 


getExpVal :: Exp -> Integer -> Computation (String, Integer, String)

getExpVal (ExpLit val) next = do
	return ("", next, show val)

getExpVal (ExpAdd a b) next = do
	(aResult, aNext, left) <- getExpVal a next 
	(bResult, bNext, right) <- getExpVal b aNext
	return (aResult ++ bResult ++ "%" ++ show bNext ++ " = add nsw i32 " ++ left ++", " ++ right ++ "\n", bNext+1, "%" ++ show bNext)

getExpVal (ExpSub a b) next = do
	(aResult, aNext, left) <- getExpVal a next 
	(bResult, bNext, right) <- getExpVal b aNext
	return (aResult ++ bResult ++ "%" ++ show bNext ++ " = sub nsw i32 " ++ left ++", " ++ right ++ "\n", bNext+1, "%" ++ show bNext)

getExpVal (ExpMul a b) next = do
	(aResult, aNext, left) <- getExpVal a next 
	(bResult, bNext, right) <- getExpVal b aNext
	return (aResult ++ bResult ++ "%" ++ show bNext ++ " = mul nsw i32 " ++ left ++", " ++ right ++ "\n", bNext+1, "%" ++ show bNext)

getExpVal (ExpDiv a b) next = do
	(aResult, aNext, left) <- getExpVal a next 
	(bResult, bNext, right) <- getExpVal b aNext
	return (aResult ++ bResult ++ "%" ++ show bNext ++ " = sdiv i32 " ++ left ++", " ++ right ++ "\n", bNext+1, "%" ++ show bNext)

getExpVal (ExpVar (Ident name)) next = do
	return ("%" ++ show next ++ " = load i32, i32* %" ++ name ++ ", align 4" ++ "\n" , next+1, "%" ++ show next)
	

compute :: [Stmt] -> String -> Integer -> String -> Computation()
compute [] result next alloc = do
	liftIO $ putStr $ header
	liftIO $ putStr $ alloc
	liftIO $ putStrLn $ "store i32 0, i32* %1"
	liftIO $ putStr $ result
	liftIO $ putStr $ footer
	return ()

compute (a:l) result0 next0 alloc0 = do
	(result, next, alloc) <- go a next0
	compute l (result0 ++ result) next (alloc0 ++ alloc)
	where
		go (SAss (Ident name) exp) next = do
			(expResult, expNext, var) <- getExpVal exp next
		 	assResult <- assignVar name
			return (expResult ++ "store i32 " ++ var ++ ", i32* %" ++ name ++ ", align 4" ++ "\n", expNext, assResult)
		go (SExp expr) next = do
			(expResult, expNext, var) <-getExpVal expr next
			return (expResult ++ "%" ++ show expNext ++ " = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 "++ var ++")\n", (expNext+1),"")


runProgram :: String -> IO()
runProgram s=
	case pProgram $ myLexer s of
	Bad a -> putStrLn a
	Ok (Prog t) ->
		evalStateT (compute t "" 2 "") (S.empty)
		
main :: IO()
main = do
	name <- getProgName
	args <- getArgs
	case args of
		[fp] -> readFile fp >>= runProgram
		_ -> getContents >>= runProgram




